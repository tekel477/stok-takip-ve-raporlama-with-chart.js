﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StokTakip_v3._0.Models;

namespace StokTakip_v3._0.Controllers
{
    public class OzelliklersController : Controller
    {
        private StokEntities1 db = new StokEntities1();

        // GET: Ozelliklers
        public ActionResult Index()
        {
            return View(db.Ozellikler.ToList());
        }

        // GET: Ozelliklers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ozellikler ozellikler = db.Ozellikler.Find(id);
            if (ozellikler == null)
            {
                return HttpNotFound();
            }
            return View(ozellikler);
        }

        // GET: Ozelliklers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Ozelliklers/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Aciklama")] Ozellikler ozellikler)
        {
            if (ModelState.IsValid)
            {
                db.Ozellikler.Add(ozellikler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ozellikler);
        }

        // GET: Ozelliklers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ozellikler ozellikler = db.Ozellikler.Find(id);
            if (ozellikler == null)
            {
                return HttpNotFound();
            }
            return View(ozellikler);
        }

        // POST: Ozelliklers/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Aciklama")] Ozellikler ozellikler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ozellikler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ozellikler);
        }

        // GET: Ozelliklers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ozellikler ozellikler = db.Ozellikler.Find(id);
            if (ozellikler == null)
            {
                return HttpNotFound();
            }
            return View(ozellikler);
        }

        // POST: Ozelliklers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ozellikler ozellikler = db.Ozellikler.Find(id);
            db.Ozellikler.Remove(ozellikler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
