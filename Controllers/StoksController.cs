﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StokTakip_v3._0.Models;

namespace StokTakip_v3._0.Controllers
{
    public class StoksController : Controller
    {
        private StokEntities1 db = new StokEntities1();

        // GET: Stoks
        public ActionResult Index()
        {
            var stok = db.Stok.Include(s => s.Model).Include(s => s.Kategori);
            return View(stok.ToList());
        }

        // GET: Stoks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stok stok = db.Stok.Find(id);
            if (stok == null)
            {
                return HttpNotFound();
            }
            return View(stok);
        }

        // GET: Stoks/Create
        public ActionResult Create()
        {
            ViewBag.markalar = db.Marka;
            ViewBag.Kategori_id = new SelectList(db.Kategori, "id", "Kategori_ad");
            return View();
        }

        // POST: Stoks/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        // [Bind(Include = "id,Ad,Stok_kodu,Adet,Alis_fiyat,Satis_fiyat,Kategori_id,Model_id,Kdv_oran")] Stok stok
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection form)
        {
            if (form !=null)
            {
                form.Remove("__RequestVerificationToken");
                form.Remove("Marka_id");
                Stok stok = new Stok();
                stok.Ad = form["Ad"];
                form.Remove("Ad");
                stok.Stok_kodu = form["Stok_kodu"];
                form.Remove("Stok_kodu");
                stok.Alis_fiyat = Convert.ToInt32(form["Alis_fiyat"]);
                form.Remove("Alis_fiyat");
                stok.Satis_fiyat = Convert.ToInt32(form["Satis_fiyat"]);
                form.Remove("Satis_fiyat");
                stok.Kategori_id = Convert.ToInt32(form["Kategori_id"]);
                form.Remove("Kategori_id");
                stok.Model_id = Convert.ToInt32(form["Model_id"]);
                form.Remove("Model_id");
                stok.Kdv_oran = Convert.ToInt32(form["Kdv_oran"]);
                form.Remove("Kdv_oran");
                stok.Adet = 0;

                var eklenen=db.Stok.Add(stok);
                db.SaveChanges();

                for(int i=0;i<form.Count;i++)
                {
                    Stok_ozellik so = new Stok_ozellik();
                    var id = form.GetKey(i);
                    var value = form[i];
                    so.iliski_id = Convert.ToInt32(id);
                    so.Stok_id = eklenen.id;
                    so.value = value;
                    db.Stok_ozellik.Add(so);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            ViewBag.Model_id = new SelectList(db.Model, "id", "Model_ad");
            ViewBag.Kategori_id = new SelectList(db.Kategori, "id", "Kategori_ad");
            return View();
        }

        // GET: Stoks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stok stok = db.Stok.Find(id);
            if (stok == null)
            {
                return HttpNotFound();
            }
            ViewBag.markalar = db.Marka;
            ViewBag.Kategori_id = new SelectList(db.Kategori, "id", "Kategori_ad", stok.Kategori_id);
            return View(stok);
        }

        // POST: Stoks/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection form)
        {
            if (form !=null)
            {
                form.Remove("__RequestVerificationToken");
                form.Remove("Marka_id");
                form.Remove("Adet");
                var stok = db.Stok.Find(Convert.ToInt32(form["id"].ToString()));
                var eskikategoriid = stok.Kategori_id;
                form.Remove("id");
                stok.Ad = form["Ad"];
                form.Remove("Ad");
                stok.Stok_kodu = form["Stok_kodu"];
                form.Remove("Stok_kodu");
                stok.Alis_fiyat = Convert.ToInt32(form["Alis_fiyat"]);
                form.Remove("Alis_fiyat");
                stok.Satis_fiyat = Convert.ToInt32(form["Satis_fiyat"]);
                form.Remove("Satis_fiyat");
                stok.Kategori_id = Convert.ToInt32(form["Kategori_id"]);
                form.Remove("Kategori_id");
                stok.Model_id = Convert.ToInt32(form["Model_id"]);
                form.Remove("Model_id");
                stok.Kdv_oran = Convert.ToInt32(form["Kdv_oran"]);
                form.Remove("Kdv_oran");
                stok.Adet = 0;

                db.Entry(stok).State = EntityState.Modified;
                db.SaveChanges();

                if (eskikategoriid != stok.Kategori_id)
                {
                    var so = db.Stok_ozellik.Where(w => w.Stok_id == stok.id && w.Ozellik_iliski.kategori_id ==eskikategoriid);
                    db.Stok_ozellik.RemoveRange(so);
                    db.SaveChanges();
                }

                for (int i = 0; i < form.Count; i++)
                {
                    var ids = form.GetKey(i);
                    var id = Convert.ToInt32(ids);
                    var value = form[i];
                    var so = db.Stok_ozellik.Where(w => w.Stok_id == stok.id && w.iliski_id == id).FirstOrDefault();
                    if (so != null)
                    {
                        so.iliski_id = id;
                        so.Stok_id = stok.id;
                        so.value = value;
                        db.Entry(so).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        Stok_ozellik sto = new Stok_ozellik();
                        sto.iliski_id = id;
                        sto.Stok_id = stok.id;
                        sto.value = value;
                        db.Stok_ozellik.Add(sto);
                        db.SaveChanges();
                    }
                }
            }
            return RedirectToAction("Index");
        }

        // GET: Stoks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stok stok = db.Stok.Find(id);
            if (stok == null)
            {
                return HttpNotFound();
            }
            return View(stok);
        }

        // POST: Stoks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Stok stok = db.Stok.Find(id);
            db.Stok.Remove(stok);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult modeller(int? markaid)
        {
            var output = db.Model.Where(w => w.Marka_id == markaid).Select(s => new { s.id, s.Model_ad, s.Marka_id }).ToArray();

            return Json(output, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ozellikler(int? katid)
        {
            var output = db.Ozellik_iliski.Where(w => w.kategori_id == katid).Select(s => new { s.id, name=s.Ozellikler.Aciklama }).ToArray();

            return Json(output, JsonRequestBehavior.AllowGet);
        }

        public JsonResult stokozellikler(int? katid,int stokid)
        {
            var output = db.Ozellik_iliski.Where(w => w.kategori_id == katid).Select(s => new { s.id, name = s.Ozellikler.Aciklama,value=s.Stok_ozellik.Where(w=>w.Stok_id==stokid).Select(ss=>ss.value).FirstOrDefault() }).ToArray();

            return Json(output, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
