﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using StokTakip_v3._0.Models;

namespace StokTakip_v3._0.Controllers
{
    public class CarisController : Controller
    {
        private StokEntities1 db = new StokEntities1();

        // GET: Caris
        public ActionResult Index()
        {
            var cari = db.Cari.Include(c => c.ilce);
            return View(cari.ToList());
        }

        // GET: Caris/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cari cari = db.Cari.Find(id);
            if (cari == null)
            {
                return HttpNotFound();
            }
            return View(cari);
        }

        // GET: Caris/Create
        public ActionResult Create()
        {
            ViewBag.iller = db.il;
            //ViewBag.ılce_id = new SelectList(db.ilce, "id", "ad");
            return View();
        }

        // POST: Caris/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Cari cari)
        {
            if (ModelState.IsValid)
            {
                
                db.Cari.Add(cari);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ılce_id = new SelectList(db.ilce, "id", "ad", cari.ılce_id);
            ViewBag.iller = db.il;
            return View(cari);
        }

        // GET: Caris/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cari cari = db.Cari.Find(id);
            if (cari == null)
            {
                return HttpNotFound();
            }
            ViewBag.iller = db.il;
            return View(cari);
        }

        // POST: Caris/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Cari_ad,Cari_soyad,FirmaUnvani,Adres,Telefon,ılce_id")] Cari cari)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cari).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ılce_id = new SelectList(db.ilce, "id", "ad", cari.ılce_id);
            return View(cari);
        }

        // GET: Caris/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cari cari = db.Cari.Find(id);
            if (cari == null)
            {
                return HttpNotFound();
            }
            return View(cari);
        }

        // POST: Caris/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cari cari = db.Cari.Find(id);
            db.Cari.Remove(cari);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        
        public JsonResult Ilceler(int? ilid)
        {
            var output=db.ilce.Where(w => w.il_id == ilid).Select(s=>new { s.id,s.ad, s.il_id }).ToArray();
            
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
