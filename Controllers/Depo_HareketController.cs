﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StokTakip_v3._0.Models;

namespace StokTakip_v3._0.Controllers
{
    public class Depo_HareketController : Controller
    {
        private StokEntities1 db = new StokEntities1();

        // GET: Depo_Hareket
        public ActionResult Index()
        {
            var depo_Hareket = db.Depo_Hareket.Include(d => d.Depo).Include(d => d.Depo1).Include(d => d.Stok);
            return View(depo_Hareket.ToList());
        }

        // GET: Depo_Hareket/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Depo_Hareket depo_Hareket = db.Depo_Hareket.Find(id);
            if (depo_Hareket == null)
            {
                return HttpNotFound();
            }
            return View(depo_Hareket);
        }

        // GET: Depo_Hareket/Create
        public ActionResult Create()
        {
            ViewBag.Giris_depo = new SelectList(db.Depo, "id", "Depo_ad");
            ViewBag.Cikis_depo = new SelectList(db.Depo, "id", "Depo_ad");
            ViewBag.Stok_id = new SelectList(db.Stok, "id", "Ad");
            return View();
        }

        // POST: Depo_Hareket/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Giris_depo,Stok_id,Miktar,Tarih,Cikis_depo")] Depo_Hareket depo_Hareket)
        {
            if (ModelState.IsValid)
            {
                depo_Hareket.Tarih = DateTime.Now;
                db.Depo_Hareket.Add(depo_Hareket);
                db.SaveChanges();
                var girendepo = db.Depo_Takip.Where(w => w.Depo_id == depo_Hareket.Giris_depo && w.Stok_id==depo_Hareket.Stok_id).FirstOrDefault();
                if (girendepo != null)
                {
                    girendepo.Stok_miktar += depo_Hareket.Miktar;
                    db.SaveChanges();
                }
                else
                {
                    Depo_Takip dpt = new Depo_Takip();
                    dpt.Depo_id = depo_Hareket.Giris_depo;
                    dpt.Stok_id = depo_Hareket.Stok_id;
                    dpt.Stok_miktar = depo_Hareket.Miktar;
                    db.Depo_Takip.Add(dpt);
                    db.SaveChanges();
                }
                var cikandepo = db.Depo_Takip.Where(w => w.Depo_id == depo_Hareket.Cikis_depo && w.Stok_id == depo_Hareket.Stok_id).FirstOrDefault();
                if (cikandepo != null)
                {
                    cikandepo.Stok_miktar -= depo_Hareket.Miktar;
                    db.SaveChanges();
                }
                else
                {
                    Depo_Takip dpt = new Depo_Takip();
                    dpt.Depo_id = depo_Hareket.Giris_depo;
                    dpt.Stok_id = depo_Hareket.Stok_id;
                    dpt.Stok_miktar = -depo_Hareket.Miktar;
                    db.Depo_Takip.Add(dpt);
                    db.SaveChanges();
                }
                
                return RedirectToAction("Index");
            }

            ViewBag.Giris_depo = new SelectList(db.Depo, "id", "Depo_ad", depo_Hareket.Giris_depo);
            ViewBag.Cikis_depo = new SelectList(db.Depo, "id", "Depo_ad", depo_Hareket.Cikis_depo);
            ViewBag.Stok_id = new SelectList(db.Stok, "id", "Ad", depo_Hareket.Stok_id);
            return View(depo_Hareket);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
