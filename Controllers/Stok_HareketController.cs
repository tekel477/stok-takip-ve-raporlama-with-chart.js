﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StokTakip_v3._0.Models;

namespace StokTakip_v3._0.Controllers
{
    public class Stok_HareketController : Controller
    {
        private StokEntities1 db = new StokEntities1();

        // GET: Stok_Hareket
        public ActionResult Index()
        {
            var stok_Hareket = db.Stok_Hareket.Include(s => s.Cari).Include(s => s.Depo).Include(s => s.Stok);
            return View(stok_Hareket.ToList());
        }

        // GET: Stok_Hareket/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stok_Hareket stok_Hareket = db.Stok_Hareket.Find(id);
            if (stok_Hareket == null)
            {
                return HttpNotFound();
            }
            return View(stok_Hareket);
        }

        // GET: Stok_Hareket/Create
        public ActionResult Create()
        {
            ViewBag.cari_id = new SelectList(db.Cari, "id", "Cari_ad");
            ViewBag.Depo_id = new SelectList(db.Depo, "id", "Depo_ad");
            ViewBag.stok_id = new SelectList(db.Stok, "id", "Ad");
            return View();
        }

        // POST: Stok_Hareket/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,cari_id,stok_id,Miktar,Fiyat,islem,Depo_id")] Stok_Hareket stok_Hareket)
        {
            if (ModelState.IsValid)
            {
                stok_Hareket.Tarih = DateTime.Now;
                db.Stok_Hareket.Add(stok_Hareket);
                db.SaveChanges();
                var stok = db.Stok.Where(w => w.id == stok_Hareket.stok_id).FirstOrDefault();
                var depotakip = db.Depo_Takip.Where(w => w.Depo_id == stok_Hareket.Depo_id && w.Stok_id == stok_Hareket.stok_id).FirstOrDefault();
                if (stok_Hareket.islem == true)
                {
                    stok.Adet -= stok_Hareket.Miktar;
                    db.Entry(stok).State = EntityState.Modified;
                    db.SaveChanges();
                    if (depotakip != null)
                    {
                        depotakip.Stok_miktar -= stok_Hareket.Miktar;
                        db.Entry(depotakip).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        Depo_Takip dt = new Depo_Takip();
                        dt.Depo_id = stok_Hareket.Depo_id;
                        dt.Stok_id = stok_Hareket.stok_id;
                        dt.Stok_miktar = -stok_Hareket.Miktar;
                        db.Depo_Takip.Add(dt);
                        db.SaveChanges();
                    } 
                }
                else
                {
                    stok.Adet += stok_Hareket.Miktar;
                    db.Entry(stok).State = EntityState.Modified;
                    db.SaveChanges();
                    if (depotakip != null)
                    {
                        depotakip.Stok_miktar += stok_Hareket.Miktar;
                        db.Entry(depotakip).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        Depo_Takip dt = new Depo_Takip();
                        dt.Depo_id = stok_Hareket.Depo_id;
                        dt.Stok_id = stok_Hareket.stok_id;
                        dt.Stok_miktar = stok_Hareket.Miktar;
                        db.Depo_Takip.Add(dt);
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("index");
            }

            ViewBag.cari_id = new SelectList(db.Cari, "id", "Cari_ad", stok_Hareket.cari_id);
            ViewBag.Depo_id = new SelectList(db.Depo, "id", "Depo_ad", stok_Hareket.Depo_id);
            ViewBag.stok_id = new SelectList(db.Stok, "id", "Ad", stok_Hareket.stok_id);
            return View(stok_Hareket);
        }

        // GET: Stok_Hareket/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stok_Hareket stok_Hareket = db.Stok_Hareket.Find(id);
            if (stok_Hareket == null)
            {
                return HttpNotFound();
            }
            ViewBag.cari_id = new SelectList(db.Cari, "id", "Cari_ad", stok_Hareket.cari_id);
            ViewBag.Depo_id = new SelectList(db.Depo, "id", "Depo_ad", stok_Hareket.Depo_id);
            ViewBag.stok_id = new SelectList(db.Stok, "id", "Ad", stok_Hareket.stok_id);
            return View(stok_Hareket);
        }

        // POST: Stok_Hareket/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,cari_id,stok_id,Miktar,Fiyat,islem,Depo_id,Tarih")] Stok_Hareket stok_Hareket)
        {
            if (ModelState.IsValid)
            {
                setStokmiktar(stok_Hareket.id, stok_Hareket.islem,stok_Hareket.Miktar);
                db.Entry(stok_Hareket).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.cari_id = new SelectList(db.Cari, "id", "Cari_ad", stok_Hareket.cari_id);
            ViewBag.Depo_id = new SelectList(db.Depo, "id", "Depo_ad", stok_Hareket.Depo_id);
            ViewBag.stok_id = new SelectList(db.Stok, "id", "Ad", stok_Hareket.stok_id);
            return View(stok_Hareket);
        }

        void setStokmiktar(int shid,bool? yeniislem, int? miktar)
        {
            var eskiStokHareket = db.Stok_Hareket.AsNoTracking().Where(w=>w.id==shid).FirstOrDefault();
            var dt = db.Depo_Takip.Where(w => w.Stok_id == eskiStokHareket.stok_id && w.Depo_id == eskiStokHareket.Depo_id).FirstOrDefault();
            var stok = db.Stok.Where(w => w.id == eskiStokHareket.stok_id).FirstOrDefault();
            if (eskiStokHareket.islem != yeniislem)
            {
                if (yeniislem == true)
                {
                    stok.Adet -= eskiStokHareket.Miktar;
                    stok.Adet -= miktar;
                    db.Entry(stok).State = EntityState.Modified;
                    db.SaveChanges();
                    dt.Stok_miktar -= eskiStokHareket.Miktar;
                    dt.Stok_miktar -= miktar;
                    db.Entry(dt).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    stok.Adet += eskiStokHareket.Miktar;
                    stok.Adet += miktar;
                    db.Entry(stok).State = EntityState.Modified;
                    db.SaveChanges();
                    dt.Stok_miktar += eskiStokHareket.Miktar;
                    dt.Stok_miktar += miktar;
                    db.Entry(dt).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            else
            {
                if (yeniislem == true)
                {
                    stok.Adet += eskiStokHareket.Miktar;
                    stok.Adet -= miktar;
                    db.Entry(stok).State = EntityState.Modified;
                    db.SaveChanges();
                    dt.Stok_miktar += eskiStokHareket.Miktar;
                    dt.Stok_miktar -= miktar;
                    db.Entry(dt).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    stok.Adet -= eskiStokHareket.Miktar;
                    stok.Adet += miktar;
                    db.Entry(stok).State = EntityState.Modified;
                    db.SaveChanges();
                    dt.Stok_miktar -= eskiStokHareket.Miktar;
                    dt.Stok_miktar += miktar;
                    db.Entry(dt).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        // GET: Stok_Hareket/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stok_Hareket stok_Hareket = db.Stok_Hareket.Find(id);
            if (stok_Hareket == null)
            {
                return HttpNotFound();
            }
            return View(stok_Hareket);
        }

        // POST: Stok_Hareket/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Stok_Hareket stok_Hareket = db.Stok_Hareket.Find(id);
            var stok = db.Stok.Where(w => w.id == stok_Hareket.stok_id).FirstOrDefault();
            var dt = db.Depo_Takip.Where(w => w.Depo_id == stok_Hareket.Depo_id && w.Stok_id == stok_Hareket.stok_id).FirstOrDefault();
            if (stok_Hareket.islem == true)
            {
                stok.Adet += stok_Hareket.Miktar;
                db.Entry(stok).State = EntityState.Modified;
                db.SaveChanges();
                dt.Stok_miktar += stok_Hareket.Miktar;
                db.Entry(dt).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                stok.Adet -= stok_Hareket.Miktar;
                db.Entry(stok).State = EntityState.Modified;
                db.SaveChanges();
                dt.Stok_miktar -= stok_Hareket.Miktar;
                db.Entry(dt).State = EntityState.Modified;
                db.SaveChanges();
            }
            db.Stok_Hareket.Remove(stok_Hareket);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult fiyat(int stokid, bool islem)
        {
            string output = "";
            if (islem)
            {
                output = db.Stok.Where(w => w.id == stokid).Select(s => s.Satis_fiyat).FirstOrDefault().ToString();
            }
            else
            {
                output = db.Stok.Where(w => w.id == stokid).Select(s => s.Alis_fiyat).FirstOrDefault().ToString();
            }

            return Json(output, JsonRequestBehavior.AllowGet);
        }

        public JsonResult satilabilirmi(int id,int miktar)
        {
            string output = "";
            var stokmiktari = db.Stok.Where(w => w.id == id).Select(s => s.Adet).FirstOrDefault();
            if (stokmiktari >= miktar)
            {
                output = "true";
            }
            else
            {
                output = "false";
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
