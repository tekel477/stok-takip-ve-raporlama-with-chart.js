﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StokTakip_v3._0.Models;

namespace StokTakip_v3._0.Controllers
{
    public class DepoesController : Controller
    {
        private StokEntities1 db = new StokEntities1();

        // GET: Depoes
        public ActionResult Index()
        {
            return View(db.Depo.ToList());
        }

        // GET: Depoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Depo depo = db.Depo.Find(id);
            if (depo == null)
            {
                return HttpNotFound();
            }
            return View(depo);
        }

        // GET: Depoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Depoes/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Depo_ad")] Depo depo)
        {
            if (ModelState.IsValid)
            {
                db.Depo.Add(depo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(depo);
        }

        // GET: Depoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Depo depo = db.Depo.Find(id);
            if (depo == null)
            {
                return HttpNotFound();
            }
            return View(depo);
        }

        // POST: Depoes/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Depo_ad")] Depo depo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(depo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(depo);
        }

        // GET: Depoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Depo depo = db.Depo.Find(id);
            if (depo == null)
            {
                return HttpNotFound();
            }
            return View(depo);
        }

        // POST: Depoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Depo depo = db.Depo.Find(id);
            db.Depo.Remove(depo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
