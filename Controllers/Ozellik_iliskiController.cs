﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StokTakip_v3._0.Models;

namespace StokTakip_v3._0.Controllers
{
    public class Ozellik_iliskiController : Controller
    {
        private StokEntities1 db = new StokEntities1();

        // GET: Ozellik_iliski
        public ActionResult Index()
        {
            var ozellik_iliski = db.Ozellik_iliski.Include(o => o.Ozellikler).Include(o => o.Kategori);
            return View(ozellik_iliski.ToList());
        }

        // GET: Ozellik_iliski/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ozellik_iliski ozellik_iliski = db.Ozellik_iliski.Find(id);
            if (ozellik_iliski == null)
            {
                return HttpNotFound();
            }
            return View(ozellik_iliski);
        }

        // GET: Ozellik_iliski/Create
        public ActionResult Create()
        {
            ViewBag.ozellik_id = new SelectList(db.Ozellikler, "id", "Aciklama");
            ViewBag.kategori_id = new SelectList(db.Kategori, "id", "Kategori_ad");
            return View();
        }

        // POST: Ozellik_iliski/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,ozellik_id,kategori_id")] Ozellik_iliski ozellik_iliski)
        {
            if (ModelState.IsValid)
            {
                db.Ozellik_iliski.Add(ozellik_iliski);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ozellik_id = new SelectList(db.Ozellikler, "id", "Aciklama", ozellik_iliski.ozellik_id);
            ViewBag.kategori_id = new SelectList(db.Kategori, "id", "Kategori_ad", ozellik_iliski.kategori_id);
            return View(ozellik_iliski);
        }

        // GET: Ozellik_iliski/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ozellik_iliski ozellik_iliski = db.Ozellik_iliski.Find(id);
            if (ozellik_iliski == null)
            {
                return HttpNotFound();
            }
            ViewBag.ozellik_id = new SelectList(db.Ozellikler, "id", "Aciklama", ozellik_iliski.ozellik_id);
            ViewBag.kategori_id = new SelectList(db.Kategori, "id", "Kategori_ad", ozellik_iliski.kategori_id);
            return View(ozellik_iliski);
        }

        // POST: Ozellik_iliski/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,ozellik_id,kategori_id")] Ozellik_iliski ozellik_iliski)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ozellik_iliski).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ozellik_id = new SelectList(db.Ozellikler, "id", "Aciklama", ozellik_iliski.ozellik_id);
            ViewBag.kategori_id = new SelectList(db.Kategori, "id", "Kategori_ad", ozellik_iliski.kategori_id);
            return View(ozellik_iliski);
        }

        // GET: Ozellik_iliski/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ozellik_iliski ozellik_iliski = db.Ozellik_iliski.Find(id);
            if (ozellik_iliski == null)
            {
                return HttpNotFound();
            }
            return View(ozellik_iliski);
        }

        // POST: Ozellik_iliski/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ozellik_iliski ozellik_iliski = db.Ozellik_iliski.Find(id);
            db.Ozellik_iliski.Remove(ozellik_iliski);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
