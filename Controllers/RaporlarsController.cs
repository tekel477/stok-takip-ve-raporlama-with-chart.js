﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using StokTakip_v3._0.Models;

namespace StokTakip_v3._0.Controllers
{
    public class RaporlarsController : Controller
    {
        private StokEntities1 db = new StokEntities1();
        // GET: Raporlars
        public ActionResult Index()
        {
            ViewBag.iller = db.il;
            ViewBag.stoks = db.Stok;
            ViewBag.caris = db.Cari;
            ViewBag.depos = db.Depo;
            ViewBag.markas = db.Marka;
            ViewBag.models = db.Model;
            ViewBag.kategoris = db.Kategori;
            return View();
        }
        public JsonResult Ilceler(int? ilid)
        {
            var output = db.ilce.Where(w => w.il_id == ilid).Select(s => new { s.id, s.ad, s.il_id }).ToArray();

            return Json(output, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AlisStokRapor(string date1, string date2)
        {
            var firstDate = DateTime.Parse(date1);
            var lastDate = DateTime.Parse(date2);
            var output = db.Stok_Hareket.Where(w => w.islem ==false && w.Tarih>=firstDate && w.Tarih<=lastDate ).Select(s => new {s.Stok.id, s.Stok.Stok_kodu, s.Stok.Ad, s.Cari.Cari_ad,s.Depo.Depo_ad,s.Fiyat,s.Miktar,s.Tarih }).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SatisStokRapor(string date1, string date2)
        {
            var firstDate = DateTime.Parse(date1);
            var lastDate = DateTime.Parse(date2);
            var output = db.Stok_Hareket.Where(w => w.islem == true && w.Tarih>= firstDate && w.Tarih<= lastDate).Select(s => new { s.stok_id, s.Stok.Stok_kodu, s.Stok.Ad, s.Cari.Cari_ad, s.Depo.Depo_ad, s.Fiyat, s.Miktar,s.Tarih }).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult MarkaSatisRapor(string date1, string date2,int ? markaid)
        {
            var firstDate = DateTime.Parse(date1);
            var lastDate = DateTime.Parse(date2);
            var output = db.Stok_Hareket.Where(w => w.islem == true && w.Tarih >= firstDate && w.Tarih <= lastDate && w.Stok.Model.Marka_id == markaid).Select(s => new { s.Stok.Model.Marka_id, s.Stok.Model.Marka.Marka_ad, s.Stok.Ad, s.Stok.Stok_kodu, s.Depo.Depo_ad, s.Fiyat, s.Miktar,s.Tarih,s.Cari.Cari_ad,s.stok_id}).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult ModelSatisRapor(string date1, string date2,int modelid)
        {
            var firstDate = DateTime.Parse(date1);
            var lastDate = DateTime.Parse(date2);
            var output = db.Stok_Hareket.Where(w => w.islem == true && w.Tarih >= firstDate && w.Tarih <= lastDate && w.Stok.Model_id == modelid).Select(s => new { s.Stok.Model_id, s.Stok.Stok_kodu, s.Stok.Model.Model_ad, s.Stok.Ad, s.Miktar, s.Fiyat, s.Depo.Depo_ad,s.Tarih,s.id}).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult MarkaAlisRapor(string date1, string date2, int markaid)
        {
            var firstDate = DateTime.Parse(date1);
            var lastDate = DateTime.Parse(date2);
            var output = db.Stok_Hareket.Where(w => w.islem == false && w.Tarih >= firstDate && w.Tarih <= lastDate && w.Stok.Model.Marka_id==markaid).Select(s => new { s.Stok.Model.Marka_id, s.Stok.Model.Marka.Marka_ad, s.Stok.Ad, s.Stok.Stok_kodu, s.Depo.Depo_ad, s.Fiyat, s.Miktar,s.Cari.Cari_ad,s.Tarih}).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ModelAlisRapor(string date1, string date2, int modelid)
        {
            var firstDate = DateTime.Parse(date1);
            var lastDate = DateTime.Parse(date2);
            var output = db.Stok_Hareket.Where(w => w.islem == false && w.Tarih >= firstDate && w.Tarih <= lastDate && w.Stok.Model_id==modelid).Select(s => new { s.Stok.Model_id, s.Stok.Stok_kodu, s.Stok.Model.Model_ad, s.Stok.Ad, s.Miktar, s.Fiyat, s.Depo.Depo_ad,s.Tarih }).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MarkaStokRapor(int markaid)
        {
            var output = db.Stok.Where(w => w.Model.Marka_id == markaid).Select(s => new { s.Model_id, s.Stok_kodu, s.Model.Marka.Marka_ad, s.Ad, s.Adet, s.Alis_fiyat, s.Kdv_oran, s.Satis_fiyat,s.id}).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ModelStokRapor(int modelid)
        {
            var output = db.Stok.Where(w => w.Model_id == modelid).Select(s => new { s.Model_id, s.Stok_kodu, s.Model.Model_ad, s.Ad, s.Adet, s.Alis_fiyat, s.Kdv_oran, s.Satis_fiyat,s.id }).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DepoStokRapor(int depoid)
        {
            var output = db.Depo_Takip.Where(w => w.Depo_id == depoid).Select(s => new { s.Stok.Model_id, s.Stok.Stok_kodu, s.Stok.Model.Model_ad, s.Stok.Ad, s.Stok.Alis_fiyat, s.Stok.Satis_fiyat,s.Stok_miktar, s.Depo.Depo_ad,s.Stok_id}).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        public JsonResult StokDepoRapor(int stokid)
        {
            var output = db.Depo_Takip.Where(w => w.Stok_id == stokid).Select(s => new { s.Stok.Stok_kodu, s.Stok.Ad, s.Stok_miktar, s.Depo.Depo_ad,s.Depo_id }).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CariStokRapor(string date1, string date2, int cariid)
        {
            var firstDate = DateTime.Parse(date1);
            var lastDate = DateTime.Parse(date2);
            var output = db.Stok_Hareket.Where(w => w.Tarih >= firstDate && w.Tarih <= lastDate && w.cari_id == cariid).Select(s => new { s.Stok.Model_id, s.Stok.Stok_kodu, s.Stok.Model.Model_ad, s.Stok.Ad, s.Miktar, s.Fiyat, s.Depo.Depo_ad,s.Tarih,s.islem,s.stok_id}).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult StokCariRapor(string date1, string date2, int stokid)
        {
            var firstDate = DateTime.Parse(date1);
            var lastDate = DateTime.Parse(date2);
            var output = db.Stok_Hareket.Where(w => w.Tarih >= firstDate && w.Tarih <= lastDate && w.stok_id == stokid).Select(s => new {s.cari_id, s.Cari.Cari_ad,s.Cari.Cari_soyad,s.Cari.Telefon,s.Cari.FirmaUnvani, s.Stok.Stok_kodu, s.Stok.Ad, s.Miktar, s.Fiyat, s.Depo.Depo_ad,s.Tarih,s.islem }).ToArray();
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ilceCariRapor(int ilid, int ilceid)
        {
            if (ilceid == 0)
            {
                var output = db.Cari.Where(w => w.ilce.il_id == ilid).Select(s => new { s.id, s.Cari_ad, s.Cari_soyad, s.Telefon, s.FirmaUnvani, s.ilce.il.ad, s.Adres, ilcead = s.ilce.ad }).ToArray(); //ilçe adı yazılmıyor
                return Json(output, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var output = db.Cari.Where(w => w.ılce_id == ilceid).Select(s => new { s.id, s.Cari_ad, s.Cari_soyad, s.Telefon, s.FirmaUnvani,s.ilce.il.ad,s.Adres,ilcead=s.ilce.ad}).ToArray(); //ilçe adı yazılmıyor
                return Json(output, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ilceSatisRapor(string date1, string date2,int ilid, int ilceid)
        {
            var firstDate = DateTime.Parse(date1);
            var lastDate = DateTime.Parse(date2);
            if (ilceid == 0)
            {
                var output = db.Stok_Hareket.Where(w => w.islem == true && w.Tarih >= firstDate && w.Tarih <= lastDate && w.Cari.ilce.il_id == ilid).Select(s => new { s.Stok.Model_id, s.Stok.Stok_kodu, s.Stok.Model.Model_ad, s.Stok.Ad, s.Miktar, s.Fiyat, s.Depo.Depo_ad, s.Tarih,s.Cari.Cari_ad,s.Cari.Cari_soyad,s.Cari.ilce.ad}).ToArray();
                return Json(output, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var output = db.Stok_Hareket.Where(w => w.islem == true && w.Tarih >= firstDate && w.Tarih <= lastDate && w.Cari.ılce_id == ilceid).Select(s => new { s.Stok.Model_id, s.Stok.Stok_kodu, s.Stok.Model.Model_ad, s.Stok.Ad, s.Miktar, s.Fiyat, s.Depo.Depo_ad, s.Tarih, s.Cari.Cari_ad, s.Cari.Cari_soyad, s.Cari.ilce.ad}).ToArray();
                return Json(output, JsonRequestBehavior.AllowGet);
            }
            
        }
        public JsonResult KategoriStokRapor(int kategoriid)
        {
            var check = true;
            ArrayList checkids = new ArrayList();
            ArrayList totalids = new ArrayList();
            checkids.Add(kategoriid);
            while (check)
            {
                totalids.AddRange(checkids);
                ArrayList ids = new ArrayList();
                foreach (int item in checkids)
                {
                    var getid = db.Kategori.Where(w => w.Ust_kat_id == item && w.id != 1).Select(s=>s.id).ToList();
                    ids.AddRange(getid);
                }
                
                if (ids.Count > 0)
                {
                    checkids.Clear();
                    checkids.AddRange(ids);
                }
                else
                {
                    check = false;
                }
            }
            ArrayList output = new ArrayList();
            foreach(int item in totalids)
            {
                var stoks = db.Stok.Where(w => w.Kategori_id == item).Select(s=> new { s.Model_id, s.Stok_kodu, s.Model.Model_ad, s.Ad, s.Adet,s.Alis_fiyat,s.Kdv_oran,s.Satis_fiyat,s.Kategori.Kategori_ad }).ToArray();
                output.AddRange(stoks);
            }
            return Json(output, JsonRequestBehavior.AllowGet);
        }
        


    }
}