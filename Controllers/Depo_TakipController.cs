﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StokTakip_v3._0.Models;

namespace StokTakip_v3._0.Controllers
{
    public class Depo_TakipController : Controller
    {
        private StokEntities1 db = new StokEntities1();

        // GET: Depo_Takip
        public ActionResult Index()
        {
            var depo_Takip = db.Depo_Takip.Include(d => d.Depo).Include(d => d.Stok);
            return View(depo_Takip.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
