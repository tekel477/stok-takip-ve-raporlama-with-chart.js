﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using StokTakip_v3._0.Models;

namespace StokTakip_v3._0.Controllers
{
    public class SecurityController : Controller
    {
        private StokEntities1 db = new StokEntities1();
        [AllowAnonymous]
        public ActionResult Kayit()
        {
            return View();

        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Kayit(Kullanici Model)

        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Kayit", "Security");
            }
            db.Kullanici.Add(Model);
            db.SaveChanges();
            return RedirectToAction("Login", "Security");
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Kullanici kullanici)
        {
            if (ModelState.IsValid)
            {
                var kayit = db.Kullanici.Where(w => w.kullanici_adi == kullanici.kullanici_adi && w.sifre == kullanici.sifre).FirstOrDefault();
                if (kullanici != null)
                {
                    FormsAuthentication.SetAuthCookie(kullanici.kullanici_adi, false);
                    Session["KULLANICI"] = kullanici.kullanici_adi;
                    return RedirectToAction("index", "Home");
                }  
            }
            ViewBag.HATA = "Kullanıcı Adı veya Şifre Yanlış..!";
            return View();
        }
        // GET: Security/Create

        public ActionResult Cikis()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Security");
        }
        public ActionResult Create()
        {
            return View();
        }

        // POST: Security/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,kullanici_adi,sifre")] Kullanici kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Kullanici.Add(kullanici);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kullanici);
        }

        // GET: Security/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = db.Kullanici.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        // POST: Security/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,kullanici_adi,sifre")] Kullanici kullanici)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kullanici).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kullanici);
        }

        // GET: Security/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kullanici kullanici = db.Kullanici.Find(id);
            if (kullanici == null)
            {
                return HttpNotFound();
            }
            return View(kullanici);
        }

        // POST: Security/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kullanici kullanici = db.Kullanici.Find(id);
            db.Kullanici.Remove(kullanici);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
