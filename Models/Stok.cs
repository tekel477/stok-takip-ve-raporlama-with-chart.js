//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StokTakip_v3._0.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public partial class Stok
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Stok()
        {
            this.Depo_Hareket = new HashSet<Depo_Hareket>();
            this.Depo_Takip = new HashSet<Depo_Takip>();
            this.Stok_ozellik = new HashSet<Stok_ozellik>();
            this.Stok_Hareket = new HashSet<Stok_Hareket>();
        }
    
        public int id { get; set; }
        [DisplayName("Stok Kodu")]
        public string Stok_kodu { get; set; }
        public Nullable<int> Adet { get; set; }
        [DisplayName("Al�� Fiyat�")]
        public Nullable<int> Alis_fiyat { get; set; }
        [DisplayName("Sat�� Fiyat�")]
        public Nullable<int> Satis_fiyat { get; set; }
        [DisplayName("Kategori Id")]
        public int Kategori_id { get; set; }
        [DisplayName("Model Id")]
        public int Model_id { get; set; }
        [DisplayName("KDV Oran�")]
        public Nullable<int> Kdv_oran { get; set; }
        public string Ad { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Depo_Hareket> Depo_Hareket { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Depo_Takip> Depo_Takip { get; set; }
        public virtual Kategori Kategori { get; set; }
        public virtual Model Model { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Stok_ozellik> Stok_ozellik { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Stok_Hareket> Stok_Hareket { get; set; }
    }
}
